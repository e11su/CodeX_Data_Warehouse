from datetime import datetime
import pymysql

#
#       КЛАСС ОБРАБОТКИ ОШИБОК
#

class Errors(Exception):
    def __init__(self, *args):
        if args:
            self.message = args[0]
        else:
            self.message = None

    def __str__(self):
        print(' calling str')
        if self.message:
            return 'Error, {0}'.format(self.message)
        else:
            return 'Error has been raised'

#
#       КЛАСС СОЗДАНИЯ ОЧЕРЕДЕЙ ЗАПРОСОВ НА СЕРВЕР
#

class query():
    def __init__(self):

        #       ДЕФОЛТНЫЕ ПАРАМЕТРЫ РАБОЧЕЙ ТАБЛИЦЫ И ИСТОЧНИКА ДАННЫХ

        self.table = 'db_commit'

        #       ПАРАМЕТР id ПОСЛЕДНЕГО ДОПОЛНЕНИЯ ДАННЫХ СЕРВЕРА

        self.last_id = int

        #       СПИСКИ СУЩЕСТВУЮЩИХ ПАРАМЕТРОВ ИСТОЧНИКА ДАННЫХ И ТИПА ДАННЫХ

        self.db_commit_src = ['Frontend', 'Backend', 'CRM_ERP', 'dBases']
        self.src_data_types = ['json', 'text']

    #       ФУНКЦИЯ ПОЛУЧЕНИЯ ДАННЫХ КОНКРЕТНОЙ ТАБЛИЦЫ

    def SQL_fetch(self, id):
        self.cursor = pymysql.connect(host='127.0.0.1',
                        user='root', 
                        password='Egoramurin1!',  
                        db='CodeX').cursor()

        #       ВАРИАНТЫ ПОЛУЧЕНИЯ ДАННЫХ *
        if id:
            self.cursor.execute(f'SELECT * FROM db_commit WHERE id_commit = {id}')

        else:
            self.cursor.execute(f'SELECT * FROM db_commit WHERE 1')

        data = self.cursor.fetchall()

        return data

    #       ФУНКЦИЯ ЗАПИСИ ДАННЫХ НА СЕРВЕР

    def SQL_commit(self, src, data_type, data):
        self.connect = pymysql.connect(host='127.0.0.1',
                                user='root', 
                                password='Egoramurin1!',    
                                db='CodeX')

        #       ОПРЕДЕЛЕНИЕ ФОРМАТА ДАННЫХ (JSON  ИЛИ  FILE)

        if data.count('}') >= 1:
            if data_type in self.src_data_types:
                if (src != ''):
                    self.connect.cursor().execute(f'''INSERT INTO db_commit (id_commit, src, data_type, data, time_r) values(NULL, '{src}', '{data_type}', '{data}', NOW())''')
                
                else:
                    raise(Errors('No_Source_Error'))
                
            else:
                raise(Errors('Wrong_Data_Type_Error'))

        #       ДАННЫЕ В ВИДЕ ПУТИ ФАЙЛА

        else:
            f_data = []
            with open(data) as f:
                f_data = f.readlines()
                f.close()

            str_data = ''
            for i in f_data:
                str_data += i

            #       ОПРЕДЕЛЕНИЕ СООТВЕТСИВИЯ ТИПА ДАННЫХ И ИСТОЧНИКА С СУЩЕСТВУЮЩИМИ ВАРИАНТАМИ

            if data_type in self.src_data_types:
                if (src != ''):
                    self.connect.cursor().execute(f'''INSERT INTO db_commit (id_commit, src, data_type, data, time_r) values(NULL, '{src}', '{data_type}', '{str_data}', NOW())''')
                
                else:
                    raise(Errors('No_Source_Error'))
                
            else:
                raise(Errors('Wrong_Data_Type_Error'))

            
        self.connect.commit()

    #       ФУНКЦИЯ ОБНОВЛЕНИЯ ДАННЫХ НА СЕРВЕРЕ

    def SQL_update(self, id, src, data_type, data):
        self.connect = pymysql.connect(host='127.0.0.1',
                                user='root', 
                                password='Egoramurin1!',    
                                db='CodeX')
        
        if data.count('}') >= 1:
            if data_type in self.src_data_types:
                if (src != ''):
                    self.connect.cursor().execute(f'''UPDATE db_commit SET src = '{src}', data_type = '{data_type}', data = '{data}', time_r = NOW() WHERE id_commit = {id}''')
                
                else:
                    raise(Errors('No_Source_Error'))
                
            else:
                raise(Errors('Wrong_Data_Type_Error'))

        #       ДАННЫЕ В ВИДЕ ПУТИ ФАЙЛА

        else:
            f_data = []
            with open(data) as f:
                f_data = f.readlines()
                f.close()

            str_data = ''
            for i in f_data:
                str_data += i

            #       ОПРЕДЕЛЕНИЕ СООТВЕТСИВИЯ ТИПА ДАННЫХ И ИСТОЧНИКА С СУЩЕСТВУЮЩИМИ ВАРИАНТАМИ

            if data_type in self.src_data_types:
                if (src != ''):
                    self.connect.cursor().execute(f'''UPDATE db_commit SET src = '{src}', data_type = '{data_type}', data = '{data}', time_r = NOW() WHERE id_commit = {id}''')
                
                else:
                    raise(Errors('No_Source_Error'))
                
            else:
                raise(Errors('Wrong_Data_Type_Error'))

            
        self.connect.commit()
        
    #       ФУНКЦИЯ УДАЛЕНИЯ КОММИТА ИЗ БАЗЫ ДАННЫХ

    def SQL_delete(self, id):
        self.connect = pymysql.connect(host='127.0.0.1',
                                user='root', 
                                password='Egoramurin1!',    
                                db='CodeX')
        self.connect.cursor().execute(f'''DELETE FROM db_commit WHERE id_commit = {id}''')
        
    #       ФУНКЦИЯ ОПРЕДЕЛЕНИЯ id ПОСЛЕДНЕГО ДОПОЛНЕНИЯ ДАННЫХ СЕРВЕРА

    def SQL_last_id(self):
        temp = self.SQL_fetch()
        
        try:
            self.last_id = temp[-1][0]

        except(IndexError):
            if temp:
                self.last_id = 1

            else:
                self.last_id = 0

    #       ФУНКЦИЯ ПОЛУЧЕНИЯ ИНФОРМАЦИИ О СТОЛБЦАХ

    def SQL_schema_info(self):
        self.cursor = pymysql.connect(host='127.0.0.1',
                        user='root', 
                        password='Egoramurin1!',  
                        db='CodeX').cursor()
        
        data = self.cursor.execute(f'DESCRIBE db_commit;')

        data = self.cursor.fetchall()

        return data

if __name__ == '__main__':
    z = query()

    for data_type in ['json', 'text']:
        for src in ['Frontend', 'Backend', 'CRM_ERP', 'dBases']:
            z.SQL_commit(data_type=data_type, data= './ex.JSON', src= src)
