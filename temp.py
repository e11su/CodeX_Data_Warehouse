from SQL import *
import random



def form_commit(commit):
    keys = []
    com = [{}]

    for x in q.SQL_schema_info():
        keys.append(x[0])

    data = commit

    for i in range(len(keys)):
        com[0][keys[i]] = data[i]

    return com

def get(id= 0):
    if id == 0:
        id = random.randint(1, len(q.SQL_schema_info()))
    
    try:
        commit = q.SQL_fetch(id= id)[0]
        commit = form_commit(commit)
        return commit, 200
    except:
        return "Commit not found", 404
        
def post(params):
    keys = []

    for x in q.SQL_schema_info():
        if (x[0] != "id_commit") and (x[0] != "time_r"):
            keys.append(x[0])

    q.SQL_commit(params[0], params[1], params[2])

    commit = {keys[i] : params[i] for i in range(len(keys))}

    return commit, 201

def put(id, params):
    keys = []
        
    for x in q.SQL_schema_info():
        if (x[0] != "id_commit") and (x[0] != "time_r"):
            keys.append(x[0])

    q.SQL_update(id, params[0], params[1], params[2])

    commit = {keys[i] : params[i] for i in range(len(keys))}
    commit["id"] = id

    return commit, 201

def delete(id):
    q.SQL_delete(id)
    
    return f"Quote with id {id} is deleted.", 200
     
if __name__ == '__main__':
    q = query()
    for data_type in ['json', 'text']:
        for src in ['Frontend', 'Backend', 'CRM_ERP', 'dBases']:
            params = [src, data_type, './ex.JSON']
            print(post(params))

    print(put(1, params))
    print(put(2, params))

    for i in range(5):
        print(get(i))
    
    print(delete(5))