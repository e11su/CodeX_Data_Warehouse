from flask import Flask
from flask_restful import Api, Resource, reqparse
import random
from SQL import *

app = Flask(__name__)
api = Api(app)
q = query()


class Quote(Resource):
    def form_commit(self, commit):
        keys = []
        com = [{}]

        for x in q.SQL_schema_info():
            keys.append(x[0])

        data = commit

        for i in range(len(keys)):
            com[0][keys[i]] = data[i]

        return com
    
        
    def get(self, id= 0):
        if id == 0:
            id = random.randint(1, len(q.SQL_schema_info()))
        
        try:
            commit = q.SQL_fetch(id= id)[0]
            commit = self.form_commit(commit)
            return commit, 200
        except:
            return "Commit not found", 404
        

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument("src")
        parser.add_argument("data_type")
        parser.add_argument("data")
        params = parser.parse_args()
        
        keys = []

        for x in q.SQL_schema_info():
            if (x[0] != "id_commit") and (x[0] != "time_r"):
                keys.append(x[0])

        q.SQL_commit(params[0], params[1], params[2])

        commit = {keys[i] : params[i] for i in range(len(keys))}

        return commit, 201
    
    
    def put(self, id):
        parser = reqparse.RequestParser()
        parser.add_argument("src")
        parser.add_argument("data_type")
        parser.add_argument("data")
        params = parser.parse_args()
        
        keys = []
        
        for x in q.SQL_schema_info():
            if (x[0] != "id_commit") and (x[0] != "time_r"):
                keys.append(x[0])

        q.SQL_update(id, params[0], params[1], params[2])

        commit = {keys[i] : params[i] for i in range(len(keys))}
        commit["id"] = id

        return commit, 201
    

    def delete(self, id):
        q.SQL_delete(id)
        
        return f"Quote with id {id} is deleted.", 200
    

api.add_resource(Quote, "/commits", "/commits/", "/commits/<int:id>")

if __name__ == "__main__":
    app.run(debug= True)